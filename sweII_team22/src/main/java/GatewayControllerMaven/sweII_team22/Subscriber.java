package GatewayControllerMaven.sweII_team22;

import java.util.UUID;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONObject;

/*
 * Subscriber can be constructed to connect to our Mqtt Broker on cloudmqtt.com
 * Once connected & subscribed to a 'topic,' any messages published from device simulator will cause messagedArrived to run
 */

public class Subscriber implements MqttCallback {

	private MqttClient myClient;
	private MqttType connType;

	// Subscriber Constructor
	public Subscriber(String url, String port, String username, String password, MqttType type) throws MqttException {
		connType = type;
		
		String uri = "";
		
		//Tests mqtttype doesn't require a port number. This is the broker located on the DO droplet
		if (type == MqttType.Device) {
			uri = url + ":" + port;
		}
		else if (type == MqttType.Tests) {
			uri = url;
		}
		
		myClient = createClient(uri);
		myClient.setCallback(this);

		// Set connection options
		MqttConnectOptions connOpts = new MqttConnectOptions();
		connOpts.setCleanSession(true);
		connOpts.setUserName(username);
		connOpts.setPassword(password.toCharArray());

		// Connect to broker
		System.out.println("Connecting to broker: " + uri);
		myClient.connect(connOpts);
		System.out.println("Connected");

	}
	
	public MqttType getType() {
		return connType;
	}
	
	// Returns a new mqtt client
	public static MqttClient createClient(String uri) throws MqttException {
		return new MqttClient(uri,UUID.randomUUID().toString(), new MemoryPersistence());
	}
	
	// Takes in a topic and subscribes to it.
	public void newSubscription(String topic) throws MqttException {
		myClient.subscribe(topic);
	}
	
	public void newPublish(String topic, String message) {
		MqttMessage newMessage = new MqttMessage(message.getBytes());
		try {
			myClient.publish(topic, newMessage);
			System.out.println("Message published to: " + topic);
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// Default from MqttCallback Interface
	public void connectionLost(Throwable cause) {
		System.out.println("Connection lost because: " + cause);
		System.exit(1);
	}
	
	// Default from MqttCallback Interface
	// Calls static method putHeartbeat to send back to CIC
	public void  messageArrived(String topic, MqttMessage message) throws Exception {
		String heartbeat = new String(message.getPayload());
		System.out.println(heartbeat);
		//System.out.println(String.format("[%s] %s", topic, heartbeat));

		JSONObject beatJSON = new JSONObject(heartbeat);
		if (getType() == MqttType.Device) {
			ControllerImpl.putHeartbeat("gw", beatJSON);
			
		}
		else if (getType() == MqttType.Tests) {
			
			Thread thread = new Thread(new SocketManager(8080, beatJSON));
			thread.start();
		}
	}

	// Default from MqttCallback Interface
	public void deliveryComplete(IMqttDeliveryToken token) {
		// TODO Auto-generated method stub

	}

	// Closes mqtt connection to broker
	public void closeConnection() throws MqttException {
		myClient.disconnect();
		System.out.println("Connection to broker closed");
	}
	
	// Loop through array of tests & run them all
		// 
//		private void handleTests(ArrayList<String> tests, String gwID) {
//			JSONObject payload = new JSONObject();
//			payload.put("gwID", gwID);
//			payload.put("tests", tests);
//			
//			try {
//				new SocketManager(8080, payload, testingSub);
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			HashMap<CoreType, DiagnosticRunnable> cores = new HashMap<CoreType, DiagnosticRunnable>(tests.size());
//		    ExecutorService coreExecutor = Executors.newCachedThreadPool();
//		    int sleep = 0;
//		    
//			for (int i = 0; i < tests.size(); i++) {
//			    cores.put(CoreType.Socket, new DiagnosticRunnable(CoreType.Socket, sleep, tests.get(i), gwID, testingSub));
//			    sleep = sleep + 5000;
//			}
//			
//		    cores.values().forEach(coreExecutor::execute);
//		        
//		    // Threads run for 10 seconds before turning off
//		    try {
//		        Thread.sleep(10*1000);
//		    } catch (InterruptedException e) {
//		        e.printStackTrace();
//		    }
//		        
//		    cores.values().forEach(coreRunnable -> coreRunnable.shouldRun.set(false));
//		    System.out.println("Shutting down threads");
//		        
//		    coreExecutor.shutdown();
//	}
}

//interface SocketEventListener {
//	
//	void onEvent(JSONObject payload);
//}
//
//// Callback class for SocketManager so that once tests have finished running, testingSub can publish a new Message
//class SocketEvents implements SocketEventListener {
//	
//	MqttClient client;
//	
//	public SocketEvents(MqttClient client) {
//		this.client = client;
//	}
//
//	@Override
//	public void onEvent(JSONObject payload) {
//		
//		System.out.println(payload);
//		MqttMessage newMessage = new MqttMessage(payload.toString().getBytes());
//
//		try {
//			client.publish("testComplete", newMessage);
//		} catch (MqttPersistenceException e) {
//			e.printStackTrace();
//		} catch (MqttException e) {
//			e.printStackTrace();
//		}
//	}
//}


