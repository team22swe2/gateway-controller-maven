package GatewayControllerMaven.sweII_team22;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;



public class ControllerImpl{
	
	// Called once data is received from CloudMqtt/Device Sim
	public static JSONObject putHeartbeat(String gwID, JSONObject data) {
			
		JSONObject postResponse = executePost("https://team22.softwareengineeringii.com/api/cic/sensors", "", data);
		
		return postResponse;
	}

	// Called after a diagnostic test has run
	public static int putResult(JSONObject payload) {
		JSONObject postResponse = executePost("https://team22.softwareengineeringii.com/api/cic/diagnostic", "", payload);
		System.out.println(postResponse);
				
		return postResponse != null ? postResponse.getInt("responseCode") : 400;
	}
	
	// Makes POST request and returns JSON response
	private static JSONObject executePost(String targetURL, String urlParameters, JSONObject body) {
			HttpURLConnection connection = null;
			JSONObject jsonRes;

			try {
				// Create connection
				URL url = new URL(targetURL);
				connection = (HttpURLConnection) url.openConnection();
				connection.setRequestMethod("POST");
				connection.setRequestProperty("Content-Type", "application/json");

				connection.setUseCaches(false);
				connection.setDoOutput(true);
				connection.setDoInput(true);

				// Send request
				OutputStream wr = connection.getOutputStream();
				wr.write(body.toString().getBytes());

				// Get Response
				InputStream is = connection.getInputStream();
				BufferedReader rd = new BufferedReader(new InputStreamReader(is));
				StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
				String line;
				while ((line = rd.readLine()) != null) {
					response.append(line);
					response.append('\r');
				}
				rd.close();

				// Convert response to a json object
				jsonRes = new JSONObject(response.toString());
				jsonRes.put("responseCode", connection.getResponseCode());
				
				return jsonRes;
			} catch (Exception e) {
				//e.printStackTrace();
				return null;
			} finally {
				if (connection != null) {
					connection.disconnect();
				}
			}
		}
}
