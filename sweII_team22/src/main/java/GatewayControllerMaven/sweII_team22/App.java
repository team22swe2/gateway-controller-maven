package GatewayControllerMaven.sweII_team22;




import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.eclipse.paho.client.mqttv3.MqttException;

/*
 * Will utilize a single thread keeping mqtt connection open
 * Threads will be dispatched to open socket connections & run diagnostic tests
 */

public class App
{
	// Device cloudmqtt information
	static String username = "pthwuwhk";
	static String password = "3zjFWC-1THcf";
	static String brokerUrl = "tcp://soldier.cloudmqtt.com";
    static String brokerPort = "10520";
       
    public static void main( String[] args)
    {
    	
    	
    	/*
    	 * Keeping track of threads being used by storing them in 'cores' variable
    	 * Two mqtt connections are instantiated & a GatewayController object is created
    	 */
    	
    	Subscriber deviceSub = null;
    	Subscriber testingSub = null;
    	
		try {
			
			
			testingSub = new Subscriber("tcp://team22.softwareengineeringii.com", "9005", "mark", "mark0930",
					MqttType.Tests);
			testingSub.newSubscription("test_queue");
			
			deviceSub = new Subscriber(brokerUrl, brokerPort, username, password, MqttType.Device);
			deviceSub.newSubscription("turning");
			deviceSub.newSubscription("phone");
			deviceSub.newSubscription("speed");
			deviceSub.newSubscription("acceleration");
			deviceSub.newSubscription("braking");
			
		} catch (MqttException e1) {
			e1.printStackTrace();
		}
		
		
        HashMap<CoreType, GatewayController> cores = new HashMap<CoreType, GatewayController>(1);
        ExecutorService coreExecutor = Executors.newCachedThreadPool();

        cores.put(CoreType.Gateway, new GatewayController(deviceSub, testingSub));

        cores.values().forEach(coreExecutor::execute);
        
        // Threads run for 30 seconds before turning off
        try {
            Thread.sleep(300*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        cores.values().forEach(coreRunnable -> coreRunnable.shouldRun.set(false));
        System.out.println("Shutting down threads");
        
        coreExecutor.shutdown();
        
    }


}
