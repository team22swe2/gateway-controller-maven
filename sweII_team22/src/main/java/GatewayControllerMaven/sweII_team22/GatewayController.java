package GatewayControllerMaven.sweII_team22;


import java.util.concurrent.atomic.AtomicBoolean;

import org.eclipse.paho.client.mqttv3.MqttException;


public class GatewayController implements Runnable {

	private Subscriber deviceSub;
	final AtomicBoolean shouldRun;
	private Subscriber testingSub;

	public GatewayController(Subscriber deviceSub, Subscriber testingSub) {
		this.shouldRun = new AtomicBoolean(true);
		this.deviceSub = deviceSub;
		this.testingSub = testingSub;
	}

	@Override
	public void run() {

		while (shouldRun.get()) {
			// Keeps GW running until threads are shut down
		}

		// close mqtt connections
		try {
			deviceSub.closeConnection();
			testingSub.closeConnection();
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}
}
