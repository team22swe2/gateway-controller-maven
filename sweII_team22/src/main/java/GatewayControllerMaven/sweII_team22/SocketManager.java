package GatewayControllerMaven.sweII_team22;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import org.json.JSONArray;
import org.json.JSONObject;

/*
 * SocketManager can be constructed to make connection to GW Diagnostic on port 8080
 * Sends payload that's included in constructor & sends response in controllerImpl.putResult
 */

public class SocketManager implements Runnable {
	
	private Socket client;
	private PrintWriter out;
	private JSONObject payload;

	// SocketManager Constructor
	// Makes connection with GWD python server & sends data
	public SocketManager(int port, JSONObject payload) throws IOException {

		try {
			// Initializes socket connection
			client = new Socket("localhost",port);
			this.payload = payload;
		}
		catch (IOException ex) {
			System.out.println(ex.getMessage());
		}
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			OutputStream output = client.getOutputStream();
	        out = new PrintWriter(output, true);
	        
	        // Sends payload to GW Diagnostic
	        out.println(payload);
	         
			BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
			
			// Reads response from GW Diagnostic
			String fromServer = in.readLine();
			System.out.println("Data from SocketServer: \n" + fromServer);
			JSONObject jsonReceived = new JSONObject(fromServer);
			
			sendResponse(jsonReceived);
			
		}
		catch(IOException ex) {
			System.out.println(ex.getMessage());
		}
		finally {
			try {
				client.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	// Loops through tests and sends POST for each completed test
	private void sendResponse(JSONObject res) {
//		ControllerImpl controller = new ControllerImpl();
		
		JSONArray arrJson = res.getJSONArray("test_results");
		System.out.println(arrJson);
		
		for(int i = 0; i < arrJson.length(); i++)
		    ControllerImpl.putResult(new JSONObject(arrJson.getString(i)));
	}
}
