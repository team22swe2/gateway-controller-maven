package GatewayControllerMaven.sweII_team22;


import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import junit.framework.TestCase;
import org.junit.*;

public class SubscriberTests extends TestCase {
	
	static String brokerUrl = "tcp://soldier.cloudmqtt.com";
    static String brokerPort = "10520";
    static String username = "pthwuwhk";
	static String password = "3zjFWC-1THcf";
	
	//create client connection
	@Test
	public void test_createClientConn() {
		
		String uri = brokerUrl + ":" + brokerPort;
		
		try {
			MqttClient client = Subscriber.createClient(uri);
			
			MqttConnectOptions connOpts = getOptions();
			
			client.connect(connOpts);
			
			assertTrue(client.isConnected());
			
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//create client connection & subscribe to a topic
	@Test
	public void test_impleCallback() {
		String uri = brokerUrl + ":" + brokerPort;

		MqttClient client;
		try {
			client = Subscriber.createClient(uri);
			
			Callback callback = new Callback();
			
			client.setCallback(callback);
			MqttConnectOptions connOpts = getOptions();
			
			client.connect(connOpts);
			
			client.subscribe("/hello", 1);

			
			MqttMessage message = new MqttMessage();
			message.setPayload("Hello world from Java".getBytes());
			message.setQos(1);
			
			client.publish("/hello", message);
			client.publish("/hello", message);
			client.publish("/hello", message);
			client.publish("/hello", message);

			assertTrue(callback.getMessage() != null);
			
			
		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// set & get connection options
	public MqttConnectOptions getOptions() {
		
		MqttConnectOptions connOpts = new MqttConnectOptions();
		connOpts.setCleanSession(true);
		connOpts.setUserName(username);
		connOpts.setPassword(password.toCharArray());
		
		return connOpts;
	}
}

class Callback implements MqttCallback {
	
	private String latestMessage;

	@Override
	public void connectionLost(Throwable cause) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		String newMessage = new String(message.getPayload());
		latestMessage = newMessage;
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		// TODO Auto-generated method stub
		
	}
	
	public String getMessage() {
		return latestMessage;
	}
	
}
