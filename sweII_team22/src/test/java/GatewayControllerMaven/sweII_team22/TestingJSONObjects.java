package GatewayControllerMaven.sweII_team22;

import org.json.JSONObject;

public class TestingJSONObjects {
	private JSONObject heartbeatObject;
	private JSONObject resultObject;
	
	public TestingJSONObjects() {
		heartbeatObject = new JSONObject();
		resultObject = new JSONObject();
		
		setJSON();
	}
	
	public JSONObject getJSON(String type) {
		switch(type) {
		case "heartbeat": {
			return heartbeatObject;
		}
		case "result": {
			return resultObject;
		}
		default: {
			return new JSONObject();
		}
		}
	}
	
	public void setJSON() {
		resultObject.put("gwID", "1");
		resultObject.put("testType","big test");
		resultObject.put("testResult", "huge result");
		resultObject.put("timestampStart","start time");
		resultObject.put("timestampEnd", "end time");
		resultObject.put("elapsedTime","elapsed time");
		resultObject.put("errorReport", "the error report");
		
		heartbeatObject.put("speed", 20);
		heartbeatObject.put("speedLimit", 30);
		heartbeatObject.put("isSpeeding", false);
		heartbeatObject.put("phoneUsage", 25);
		heartbeatObject.put("isBrakingFast", true);
		heartbeatObject.put("isAcceleratingFast", false);
		heartbeatObject.put("isTurningFast", false);
	}
}
