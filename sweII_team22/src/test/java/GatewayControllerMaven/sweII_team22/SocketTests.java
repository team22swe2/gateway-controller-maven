package GatewayControllerMaven.sweII_team22;
import java.io.BufferedReader;
import org.junit.*;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import org.json.JSONObject;

import junit.framework.TestCase;
public class SocketTests extends TestCase {

	TestingJSONObjects instance = new TestingJSONObjects();
	static String username = "pthwuwhk";
	static String password = "3zjFWC-1THcf";
	static String brokerUrl = "tcp://soldier.cloudmqtt.com";
    static String brokerPort = "10520";
	
	// Creates a SocketServer on a new Thread
	// Uses the main thread to instantiate a new SocketManager class
    @Test
	public void test_Socket() throws Exception {
		ServerRunnable sr = new ServerRunnable();
	    Thread serverThread = new Thread(sr);
	    serverThread.start();

	    JSONObject payload = instance.getJSON("result");
	    
	    // SocketManager Class connects the client to the port the server is listening on
	    // Sends Payload then closes
	    new SocketManager(sr.getPort(), payload);
	}
	
	
	class ServerRunnable implements Runnable {
	    private ServerSocket server;

	    public ServerRunnable() throws IOException {
	        // listen on any free port
	        server = new ServerSocket(0);
	    }

	    public void run() {
	        try {
	            while (true) {
	                Socket sock = server.accept();

	                BufferedReader in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
	                OutputStream output = sock.getOutputStream();
	    	        PrintWriter out = new PrintWriter(output, true);
	     
	                String fromClient = in.readLine();
	                System.out.println("received: " + fromClient);
	                
	                out.println(instance.getJSON("result"));
	                
	                sock.close();
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }

	    public int getPort() {
	        return server.getLocalPort();
	    }
	}

}
