package GatewayControllerMaven.sweII_team22;

import org.json.JSONObject;

import junit.framework.TestCase;
import org.junit.*;

public class ControllerTests extends TestCase {
	
	TestingJSONObjects instance = new TestingJSONObjects();


	/*
	 * PutHeartbeat Tests ------------------------------------------------------- 
	 */
	
	// Tests how putHeartbeat will be used
	@Test
	public void test_putHeartbeat() {
		JSONObject payload = instance.getJSON("heartbeat");
		JSONObject response = ControllerImpl.putHeartbeat("", payload);
		
		int responseCode = response != null ? response.getInt("responseCode") : -1;
		assertTrue(responseCode == 200 || responseCode == 201);
	}
	
	
	// Tests putHeartbeat with wrong types in the payload
	@Test
	public void test_putHeartbeat_wrongType() {
		JSONObject payload = instance.getJSON("heartbeat");
		payload.remove("speed");
		payload.put("speed", "100mph");
		JSONObject response = ControllerImpl.putHeartbeat("", payload);
		
		int responseCode = response != null ? response.getInt("responseCode") : -1;

		assertTrue(responseCode == -1);
	}
	
	
	// Tests putHeartbeat with missing data in the payload
	@Test
	public void test_putHeartbeat_badData() {
		JSONObject payload = instance.getJSON("heartbeat");
		payload.remove("speed");
		payload.remove("phoneUsage");
		JSONObject response = ControllerImpl.putHeartbeat("", payload);
		
		int responseCode = response != null ? response.getInt("responseCode") : -1;
		
		assertTrue(responseCode == 200 || responseCode == 201);
	}
	
	/*
	 * PutResult Tests--------------------------------------------------------------
	 */
	
	// Tests how putResult will be used
	@Test
	public void test_putResult() {
		JSONObject payload = instance.getJSON("result");
		
		int responseCode = ControllerImpl.putResult(payload);
		assertTrue(responseCode == 200 || responseCode == 201);
	}
	
	// Tests putResult with wrong types in the payload
	@Test
	public void test_putResult_wrongType() {
		JSONObject payload = instance.getJSON("result");
		payload.remove("testType");
		payload.put("testType", 12);
		
		int responseCode = ControllerImpl.putResult(payload);
		assertTrue(responseCode == 200 || responseCode == 201);
	}
	
	// Tests putResults with missing data in the payload
	@Test
	public void test_putResult_badData() {
		JSONObject payload = instance.getJSON("result");
		payload.remove("testType");
		payload.remove("gwID");
		int responseCode = ControllerImpl.putResult(payload);
		assertEquals(responseCode, 400);
	}
}


