package GatewayControllerMaven.sweII_team22;

import org.junit.*;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

import junit.framework.TestCase;
/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {
	
	
	// Testing entire sockettest class
	@Test
	public void test_Sockets() {
		Result result = JUnitCore.runClasses(SocketTests.class);
		
		assertTrue(result.wasSuccessful());
	}
	
	// Testing entire subscribertest class
	@Test
	public void test_Subscriber() {
		Result result = JUnitCore.runClasses(SubscriberTests.class);
		
		assertTrue(result.wasSuccessful());
	}
	
	// Testing entire controllertest class
	@Test
	public void test_Controller() {
		Result result = JUnitCore.runClasses(ControllerTests.class);
		
		assertTrue(result.wasSuccessful());
	}
}
